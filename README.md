# Algorithms
## String Algorithms
* [Leonid Volnitsky Substring search algorithm](http://volnitsky.com/project/str_search/) <- https://github.com/yandex/ClickHouse/blob/master/dbms/src/Common/Volnitsky.h

# Tools
##  Java Optimizers and Obfuscators
* [ProGuard Java Optimizer and Obfuscator](https://sourceforge.net/projects/proguard/)

# C++
## References
* http://en.cppreference.com/w/
* http://www.cplusplus.com/reference/

## Standard
* http://isocpp.org/
* https://stdcpp.ru/proposals
* https://lists.isocpp.org/mailman/listinfo.cgi/

## Blogs
* http://www.1024cores.net/
* https://www.fluentcpp.com/
* https://www.bfilipek.com/

## Online IDE/Tools
* http://quick-bench.com/
* https://godbolt.org
* https://wandbox.org/
* https://ideone.com/
* https://cppinsights.io/
 
## Telegram
* https://t.me/ProCxx
* https://t.me/supapro

<!-- 2022.12.11 Just in case GitLab doesn't delete the project due to inactivity. -->
